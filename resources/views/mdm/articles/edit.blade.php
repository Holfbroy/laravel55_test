<?php
/*
*
* cnn
* Edit.blade.php
* 24.01.2017
*
*/
?>
@extends('layouts.admin')
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
    tinymce.init({
        selector: '#content',
        height: 350,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    });
</script>
@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Редактирование статьи</div>
            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Возникли следующие ошибки при редактировании статьи:</strong>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($article, ['method' => 'PATCH','route' => ['articles.update', $article->id]]) !!}
                    <div class="form-group">
                        <label for="title">Заголовок</label>
                        <textarea type="text" name="title" class="form-control"
                                  rows="3">{{$article->title}}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group" style="position: static;">
                                {!! Form::label('visible', 'Видимость') !!}
                                <?=Form::select(
                                    'visible',
                                    [
                                        0 => 'Скрыто',
                                        1 => 'Опубликовано'
                                    ],
                                    null,
                                    [
                                        'placeholder' => 'Выберите статус публикации...',
                                        'class'       => 'form-control'
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                        </div>
                    </div>
                <div class="form-group">
                    <label for="title">Содержимое</label>
                    <textarea type="text" name="content" id="content" class="form-control"
                              rows="10">{{$article->content}}</textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Сохранить" class="btn btn-info">
                    <a href="{{ route('articles.index')  }}" class="btn btn-default">Отмена</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
