<?php
/*
*
* cnn
* show.blade.php
* 24.01.2017
*
*/
?>
@extends('layouts.admin')

@section('content')
    <div style="margin-top: 10%;" class="row">
        <div class="col-sm-12 col-md-10">
            <div class="panel panel-default">

                <div class="btn-group  btn-group-justified" role="group" aria-label="...">
                    <a class="btn btn-default" href="{{ route('articles.index')  }}">Назад</a>
                    <a href="{{ route('articles.create', ['parent_id' => $article->id]) }}" class="btn btn-primary">Добавить
                        статью</a>
                    <a class="btn btn-info" href="{{ route('articles.edit',$article->id) }}"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i>Редактировать</a>
                </div>

            </div>

            <div style="margin-top:2%;" class="panel-body">

                <p>
                    <strong>Заголовок: </strong> {{ $article->title }}
                </p>
                <p>
                    <strong>Текст: </strong> {{ $article->content  }}
                </p>
                <p><strong>Список дочерних страниц</strong></p>
                <br>
                <ul>
                    @forelse (\App\Article::getSiblingsWidget($article->id) as $sibling)
                        <li><a href="{{ route('articles.show',$sibling['id'])  }}">{{$sibling['title']}}</a></li>
                    @empty
                        <li>Нет дочерних статей</li>
                    @endforelse
                </ul>

            </div>
        </div>
    </div>
@endsection
