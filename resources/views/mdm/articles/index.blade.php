@extends('layouts.admin')

@section('content')
    <div style="margin-top: 10%;" class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Список статей
            </div>
            <div class="panel-body">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <br>
                <table class="table table-bordered table-stripped">
                    <tr>
                        <th width="20">ID</th>
                        <th>Заголовок</th>
                        <th>Контент</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    @if (count($articles) > 0)
                        @foreach ($articles as $key => $articleItem)
                            <tr>
                                <td>{{ $articleItem->id }}</td>
                                <td>{{ $articleItem->title }}</td>
                                <td>{{ str_limit($articleItem->content, \App\Article::CONTENT_ANNOTATION_SIZE) }}</td>
                                <td align="center">
                                    @if(intval($articleItem->visible) !== \App\Article::EDITABLE_FLAG)
                                        blocked
                                    @endif
                                </td>
                                <td>{{ $articleItem->text }}</td>
                                <td width="25%">
                                    <a class="btn btn-primary" href="{{ route('articles.edit',$articleItem->id) }}"><i
                                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a class="btn btn-success" href="{{ route('articles.show',$articleItem->id) }}"><i
                                                class="fa fa-eye" aria-hidden="true"></i></a>
                                    @if(!$articleItem->isRoot())
                                        {{ Form::open(['method' => 'DELETE','route' => ['articles.destroy', $articleItem->id],'style'=>'display:inline']) }}
                                        {{ Form::submit('удалить', ['class' => 'btn btn-danger']) }}
                                        {{ Form::close() }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">Статьи не найдены!</td>
                        </tr>
                    @endif
                </table>

                {{ $articles->render() }}

            </div>
        </div>
    </div>

@endsection
