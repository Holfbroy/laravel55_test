<?php
/*
*
* cnn
* create.blade.php
* 24.01.2017
*
*/
?>
@extends('layouts.admin')

@section('content')
    <div style="margin-top: 10%;" class="row">
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h2 class="sub-header">Добавление статьи</h2>

            <div class="panel panel-default">
                <div class="panel-body">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Возникли следующие ошибки при добавлении статьи:</strong>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{ Form::open(array('route' => 'articles.store','method'=>'POST')) }}

                    <div class="form-group">
                        {!! Form::label('visible', 'Видимость') !!}
                        <?=Form::select(
                            'type',
                            [
                                0 => 'Скрыто',
                                1 => 'Опубликовано'
                            ],
                            null,
                            [
                                'placeholder' => 'Выберите статус публикации...',
                                'class'       => 'form-control'
                            ]);
                        ?>
                    </div>

                    <input type="hidden" name="parent_id" value="{{$parent_id}}">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <textarea type="text" name="title" class="form-control" value=""
                                  rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">Контент</label>
                        <textarea type="text" name="content" class="form-control" value=""
                                  rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Добавить" class="btn btn-info">
                        <a href="{{ route('articles.index') }}" class="btn btn-default">Отмена</a>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>


@endsection

