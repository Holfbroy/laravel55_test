<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable()->comment = "a reference to the parent (int)";
            $table->integer('lft')->nullable()->index()->comment = "left index bound (int)";
            $table->integer('rgt')->nullable()->index()->comment = "right index bound (int)";
            $table->integer('depth')->nullable()->index()->comment = "depth or nesting level (int)";
            $table->string('title', 255);
            $table->text('content');
            $table->boolean('editable')->nullable()->unsigned()->index();
            $table->boolean('visible')->nullable()->unsigned()->index();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('articles');
    }
}
