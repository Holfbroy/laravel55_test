<?php

use App\Article;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/*
 * Nested Set keys rules
 *
 *  1. Левый ключ ВСЕГДА меньше правого;
    2. Наименьший левый ключ ВСЕГДА равен 1;
    3. Наибольший правый ключ ВСЕГДА равен двойному числу узлов;
    4. Разница между правым и левым ключом ВСЕГДА нечетное число;
    5. Если уровень узла нечетное число то тогда левый ключ ВСЕГДА нечетное число, то же самое и для четных чисел;
    6.      Ключи ВСЕГДА уникальны, вне зависимости от того правый он или левый;
 */

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title'     => $faker->sentence,
        'content'   => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'editable'  => 1,
        'visible'   => 1,
        'parent_id' => rand(1, config('app.seed_number')),
        'lft'       => $faker->unique()->randomDigit,
        'rgt'       => 1,
        'depth'     => rand(1, 3),
    ];
});
