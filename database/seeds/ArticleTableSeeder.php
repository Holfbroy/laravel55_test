<?php

use App\Article;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        /* some cleaning... */
        DB::table('articles')->truncate();

        /* Сперва сделаем корневой узел */
        $root = Article::create([
            'title'    => 'Главная страница',
            'content'  => ' это главная страница',
            'editable' => Article::EDITABLE_FLAG,
            'visible'  => Article::VISIBLE_FLAG,

        ]);

        $articlesRange = range(1, config('app.seed_number'));
        $children = [];
        /* Let`s add some  depth 1 articles */
        foreach ($articlesRange as $articleID) {
            $children[] = $root->children()->create(
                [
                    'title'    => $faker->sentence,
                    'content'  => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'editable' => 1,
                    'visible'  => 1,
                ]
            );
        }

        /* Let`s add some children articles */
        foreach ($articlesRange as $key => $subArticleID) {
            $sibling = $children[$key]->children()->create(
                [
                    'title'    => $faker->sentence,
                    'content'  => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'editable' => 1,
                    'visible'  => 1,
                ]
            );
        }
    }
}
