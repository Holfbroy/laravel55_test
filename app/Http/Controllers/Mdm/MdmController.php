<?php

namespace App\Http\Controllers\Mdm;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class MdmController extends Controller
{
    const DEFAULT_PAGINATE = 25;

    //
    public function index(Request $request)
    {
        $articles = Article::orderBy('created_at', 'ASC')->paginate(self::DEFAULT_PAGINATE);
        return view('mdm.articles.index', compact('articles'))->with('i', ($request->input('page', 1) - 1) * self::DEFAULT_PAGINATE);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create(Request $request)
    {
        if ($request->has('parent_id')) {
            return view('mdm.articles.create', ['parent_id' => intval($request->input('parent_id'))]);
        } else {
            return redirect()->route('articles.index')->with('error', 'Так создать страницу нельзя!');
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'content'   => 'required',
            'parent_id' => 'required',
        ]);

        $a = Article::create($request->all());
        $parent = Article::find(intval($request->input('parent_id')));

        if ($parent !== null) {
            $a->makeChildOf($parent);
        }

        return redirect()->route('articles.index')->with('success', 'Article is added!');
    }

    /**
     * @param Article $article
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show(Article $article)
    {
        return view('mdm.articles.show', ['article' => $article]);
    }

    /**
     * @param Article $article
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function edit(Article $article)
    {
        return view('mdm.articles.edit', compact('article', 'article'));
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Article $article)
    {
        $this->validate($request, [
            'content' => 'required',
        ]);
        $article->update($request->all());
        return redirect()->route('articles.index')->with('success', 'Article was updated!');
    }


    /**
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Article $article)
    {
        if ($article !== null) {
            if (!$article->isRoot()) {
                $article->delete();
                return redirect()->route('articles.index')->with('success', 'Article was deleted!');
            } else {
                return redirect()->route('articles.index')->with('error', 'The root should not to be deleted');
            }
        } else {
            return redirect()->route('articles.index')->with('error', 'No such article!');
        }

    }
}
