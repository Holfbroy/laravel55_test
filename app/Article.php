<?php

namespace App;

use Baum\Node;
use Illuminate\Database\Eloquent\Builder;

class Article extends Node
{
    const EDITABLE_FLAG = 1;
    const VISIBLE_FLAG = 1;
    const ROOT_LFT_KEY = 1;
    const CONTENT_ANNOTATION_SIZE = 25;

    public $fillable = [
        'title',
        'content',
        'parent_id',
        'lft',
        'rgt',
        'depth',
    ];
    protected $table = 'articles';
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public static function getSiblingsWidget($nodeID)
    {
        $siblingsCollection = null;
        $node = self::find(intval($nodeID));

        if ($node !== null) {
            $siblingsCollection = $node->getSiblings();
        }

        return $siblingsCollection;
    }

    public function scopeVisible(Builder $query)
    {
        return $query->where('visible', '=', self::VISIBLE_FLAG);
    }
}
